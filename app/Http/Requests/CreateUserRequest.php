<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ic' => 'required|exists:insurance_companies,insurance_company_id',
            'cover' =>'bail|required|exists:products,product_id',
            'title' =>'bail|required|exists:titles,title_id',
            'gender' =>'bail|required|exists:genders,gender_id',
            'email' => 'bail|required|email|unique:users,email',
            'mnum' => 'bail|unique:users,mobile_number|required|regex:/^[0]{1}[7-9]{1}[0-1]{1}[0-9]{8}/',
            'address' => 'required|bail',
            'states' => 'required|bail|exists:state,id_no',
            'lga' => 'required|bail|exists:local_govt,id_no',
            'occupation' => 'bail|required|exists:occupations,occupation_id',
            'maritalstatus' => 'bail|required|exists:marital_statuses,marital_status_id',
            'fname' => 'required|bail',
            'lname' => 'required|bail',
            'cno' => 'required|bail',
            'eno' => 'required|bail',
            'regno' => 'required|bail',
            'idno' => 'required|bail',
            'idtype' => 'bail|required|exists:identification_types,idcardtype_id',
            'p1' => 'required|same:p2|min:6|bail',
            'p2' => 'required|bail',
            'amount'=>'bail|required|regex:/^[0-9.]+/',
            'carcolor'=>'required|bail|regex:/^[a-zA-Z\\s]+/',
            'cm' =>'required|bail|exists:makes,id',
            'cmm' => 'required|bail|exists:car_models,model_id',
            'cmy' => 'required|bail|exists:car_years,car_year_id',
            'yob'=>'bail|required|regex:/^[0-9]{4}/',



        ];
    }

    public function messages()
    {
        return [
            'ic.required'    => 'Please select an insurance company',
            'ic.exists' =>'The insurance company selected does not exist in our record',


        ];
    }
}
