<?php

namespace App\Http\Controllers;

use App\Purchase;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function purchases($id){
        $user = \Auth::user();
        $purchase = Purchase::where('purchase_id','=',$id)->where('user_id','=',$user->user_id)->first();

        return view('user.purchases')->with('active','upurchase')->with('purchase',$purchase);
    }

    public function index()
    {
        $purchase = Purchase::all();
        //get all the purchases done by this customer
        return view('user.index')->with('active','upurchase')->with('purchases',$purchase);
    }

    public function retryTransactioon($id)
    {
        $user = \Auth::user();
        $purchase = Purchase::where('purchase_id','=',$id)->where('user_id','=',$user->user_id)->first();

        if($purchase == null)
        {
            return back()->with('error','purchase item was not found');
        }
        if($purchase->switch_status == null )
        {
            return back()->with('error','You can only retry or renew an initially completed transaction');
        }
        $newPurchase =$purchase->replicate();
        //$newPurchase->purchase_id = null;

        $newPurchase->date_purchased = date('Y-m-d H:i:s', time());
        $newPurchase->payment_reference = "AING" . str_random(15) . time();
        $newPurchase->switch_status = null;
        $newPurchase->switch_description = null;
        $newPurchase->switch_response_time = null;
        $newPurchase->switch_reference = null;

        if($newPurchase->save())
        {
            return redirect('/user/purchases/'.$newPurchase->purchase_id);
        }

        return back()->with('error','An error occured. Please try again later');
    }

    public function complete()
    {
        $transactionId = \Input::get('gtpay_tranx_id');
        $statusCode = \Input::get('gtpay_tranx_status_code');
        $currency = \Input::get('gtpay_tranx_curr');
        $statusMessage = \Input::get('gtpay_tranx_status_msg');
        $amount = \Input::get('gtpay_tranx_amt')/100;
        $customerEmail = \Input::get('gtpay_cust_id');
        $echoData = \Input::get('gtpay_echo_data');
        $gatewayName = \Input::get('gtpay_gway_name');
        //dd(\Input::all());
        if($statusCode == '00')
        {
            $hash = strtoupper(hash('sha512',config('insurance.merchant_id').$transactionId.config('insurance.gtpay_hashkey')));
            $url = config('insurance.gtpay_requery_url'). "mertid=".config('insurance.merchant_id').
                "&amount=" . $amount . "&tranxid=".$transactionId . "&hash=".$hash;

            $handle = fopen($url,'r');
            $answer = fgets($handle,1024);
            $response = json_decode($answer);
            //dd($response);
            if($response->ResponseCode == '00')
            {
                //then the transaction was successful
                $purchase = Purchase::where('payment_reference',$transactionId)->where('user_id',\Auth()->user()->user_id)->first();
                if($purchase == null)
                {
                    return redirect()->intended('user/purchases/error')->with('response',$response->ResponseDescription)
                        ->with('error','Transaction was not found');
                }

                //update the transaction
                $purchase->approved_amount = 0;
                $purchase->switch_status = $statusCode;
                $purchase->switch_reference = '';
                $purchase->switch_description=$statusMessage;
                $purchase->switch_response_time =date('Y-m-d H:i:s', time());
                $purchase->save();



                //fire email event to customer
               // Event::fire(new SendPaymentNotificationEmailEvent($userOffering));

                //redirect user to a page to show transaction was successful
                return redirect()->intended('user/purchases/'.$purchase->purchase_id)->with('response',$statusMessage)
                    ->with('error',$statusMessage);

            }
            $purchase = Purchase::where('payment_reference',$transactionId)->where('user_id',\Auth()->user()->user_id)->first();
            if($purchase == null)
            {
                return redirect()->intended('user/purchases/error')->with('response',$response->ResponseDescription)->with('error','Transaction was not found');
            }
            //update the transaction
            $purchase->approved_amount = $response->Amount/100;
            $purchase->switch_status = $statusCode;
            $purchase->switch_reference = $response->MerchantReference;
            $purchase->switch_description=$statusMessage;
            $purchase->switch_response_time =date('Y-m-d H:i:s', time());
            $purchase->save();



            //fire email event to customer
            // Event::fire(new SendPaymentNotificationEmailEvent($userOffering));

            //redirect user to a page to show transaction was successful
            return redirect()->intended('user/purchases/'.$purchase->purchase_id)
                ->with('response',$response->ResponseDescription)
                ->with('error','');



        }
        $purchase = Purchase::where('payment_reference',$transactionId)->where('user_id',\Auth()->user()->user_id)->first();
        if($purchase == null)
        {
            return redirect()->intended('user/purchases/error')->with('response','Transaction could not be found')->with('error','Transaction was not found');
        }
        //update the transaction
        $purchase->approved_amount = 0;
        $purchase->switch_status = $statusCode;
        $purchase->switch_reference = '';
        $purchase->switch_description=$statusMessage;
        $purchase->switch_response_time =date('Y-m-d H:i:s', time());
        $purchase->save();



        //fire email event to customer
        // Event::fire(new SendPaymentNotificationEmailEvent($userOffering));

        //redirect user to a page to show transaction was successful
        return redirect()->intended('user/purchases/'.$purchase->purchase_id)
            ->with('response',$statusMessage)
            ->with('error','');
    }

    public function payUsingCard($id)
    {
        $user = \Auth::user();
        $purchase = Purchase::where('purchase_id','=',$id)->where('user_id','=',$user->user_id)->firstorfail();
        session()->put('reference_no',$purchase->payment_reference);
        session()->put('totalamount',$purchase->premium);

        $concatData = config('insurance.merchant_id').''.$purchase->payment_reference.''.($purchase->premium * 100).''.config('insurance.currency').''.
            \Auth::user()->user_id.''.config('insurance.return_url').''.config('insurance.gtpay_hashkey');

        //dd($concatData);
        $hash = hash('sha512',$concatData);
        //dd($concatData,$hash);
        session()->put('hash',$hash);
        return view('user.paywithcard')->with('active','upurchase')->with('purchase',$purchase);
    }
}
