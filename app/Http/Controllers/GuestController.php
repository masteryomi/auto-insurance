<?php

namespace App\Http\Controllers;

use App\CarMake;
use App\CarModel;
use App\CarYear;
use App\Gender;
use App\Http\Requests\CreateUserRequest;
use App\IdentificationType;
use App\InsuranceCompany;
use App\InsuranceCompanyProduct;
use App\LocalGovernment;
use App\MaritalStatus;
use App\Occupation;
use App\Product;
use App\Purchase;
use App\State;
use App\Title;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GuestController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('guest.index')->with('active','index')->with('products',$products);
    }
    public function login()
    {
        return view('guest.login')->with('active','index');
    }
    public function register()
    {
        $ic = InsuranceCompany::all();
        $state = State::all();
        $products = Product::all();
        $ms = MaritalStatus::all();
        $cm = CarMake::all();
        $gender = Gender::all();
        $title = Title::all();
        $occupation = Occupation::all();
        $idcard = IdentificationType::all();
        $comp = compact('ic','state','products','ms','cm','gender','title','occupation','idcard');
        //print_r($comp);
        return view('guest.register')->with('active','index')->with('comp',$comp);
    }

    public function getLga($id)
    {
        $lga = LocalGovernment::where('state_id','=',$id)->get();
        return response()->json($lga);
    }

    public function login_post()
    {
        $result = array();

        $email = \Input::get('email');
        $password = \Input::get('password');

        if(empty($email))
        {
            $request['status'] = false;
            $request['message'] = 'Please input your email address';
            $request['url'] = null;
            return redirect('/login')->with('error',$request['message'])->withInput();
        }

        if(empty($password))
        {
            $request['status'] = false;
            $request['message'] = 'Please input your password';
            $request['url'] = null;
            return redirect('/login')->with('error',$request['message'])->withInput();
        }

        $user = User::where('email',$email)->first();
        if(!$user || !\Hash::check($password, $user->password))

        {
            $request['status'] = false;
            $request['message'] = 'invalid email address or password';
            $request['url'] = null;
            return redirect('/login')->with('error',$request['message'])->withInput();
        }
        if($user->status == 1)
        {
            $request['status'] = false;
            $request['message'] = 'Sorry! You have not activated your account. Kindly check your email for the activation mail';
            $request['url'] = null;
            return redirect('/login')->with('error',$request['message'])->withInput();
        }
        if($user->status == 3)
        {
            $request['status'] = false;
            $request['message'] = 'Sorry! Your account has been blocked. Please contact administrator for more information';
            $request['url'] = null;
            return redirect('/login')->with('error',$request['message'])->withInput();
        }

        \Auth::loginUsingId($user->user_id);
        $request['status'] = true;
        //$request['message'] = 'Sorry! You have not activated your account. Kindly check your email for the activation mail';
        $role = $user->role()->first();
        $request['url'] = $role->role_name."/dashboard";
        return redirect()->intended($request['url']);




    }

    public function createAccount(CreateUserRequest $request)
    {
        //create the user
        $fname = \Input::get('fname');
        $last_name = \Input::get('lname');
       // $middle_name = \Input::get('middle_name');
        $mobile_no = \Input::get('mnum');
        $role_id = \Input::get('agent') == 'Y'? config("insurance.agent_role_id") :config("insurance.default_role_id");
        $email = \Input::get('email');
        $password = \Input::get('p1');
    try {

        $user = new User();
        $user->first_name = $fname;
        $user->last_name = $last_name;
        $user->role_id = $role_id;
        $user->status = 1;
        $user->mobile_number = $mobile_no;
        $user->email = $email;
        $user->password = \Hash::make($password);
        $user->created_at = date('Y-m-d H:i:s', time());
        $user->updated_at = date('Y-m-d H:i:s', time());
        //$user->company_id = config('insurance.default_company');
        $user->confirmation_token = str_random(60) . time();
        $user->gender_id = \Input::get('gender');;
        $user->occupation_id = \Input::get('occupation');;
        $user->state_id = \Input::get('states');;
        $user->lga_id = \Input::get('lga');;
        $user->title_id = \Input::get('title');;
        $user->marital_status_id = \Input::get('maritalstatus');;
        $user->address = \Input::get('address');
        $user->yob = \Input::get('yob');

        if ($user->save()) {
            //create  a purchase for the customer
            $purchase = new Purchase();
            $purchase->user_id = $user->user_id;
            $purchase->date_purchased = date('Y-m-d H:i:s', time());

            $purchase->product_id = \Input::get('cover');
            $purchase->payment_reference = "AING" . str_random(15) . time();
            $purchase->idtype_id = \Input::get('idtype');;
            $purchase->idno = \Input::get('idno');;
            $purchase->year_id = \Input::get('cmy');;
            $purchase->car_color = \Input::get('carcolor');
            $purchase->chassis = \Input::get('cno');;
            $purchase->engine_no = \Input::get('eno');;
            $purchase->regno = \Input::get('regno');
            $purchase->estimated_amt = \Input::get('amount');;
            $purchase->insurance_company_id = \Input::get('ic');
            $purchase->model_id = \Input::get('cmm');
            $purchase->make_id = \Input::get('cm');
            $type = \Input::get('cover');
            $icid = \Input::get('ic');
            //get the premium amount and the percentage
            $product = InsuranceCompanyProduct::where('product_id', '=', $type)->where('insurance_company_id', '=', $icid)->firstorfail();
            if ($product->percentage_commission == 0) {
                $purchase->transaction_amount = $purchase->premium = $product->amount;
                $purchase->percentage = $product->percentage_commission;
            } else {
                $amt = \Input::get('amount');
                $amts = ($product->percentage_commission * $amt) / 100;
                $purchase->transaction_amount = $purchase->premium = $amts;
                $purchase->percentage = $product->percentage_commission;
            }
            \Auth::loginUsingId($user->user_id);
            if ($purchase->save()) {

                return response()->json($purchase->purchase_id);
            } else {
                return response()->json(-1);
            }

        }
        return response()->json(-2);
    }
    catch(\Illuminate\Database\QueryException $ex){
        return response()->json($ex->getMessage());
       // dd($ex->getMessage());
        // Note any method of class PDOException can be called on $ex.
    }
    }

    public function getModel($id)
    {
        $lga = CarModel::where('make_id','=',$id)->get();
        return response()->json($lga);
    }
    public function getYear($id)
    {
        $lga = CarYear::where('model_id','=',$id)->get();
        return response()->json($lga);
    }
    public function getPrice($icid,$type,$amt)
    {
        $product = InsuranceCompanyProduct::where('product_id','=',$type)->where('insurance_company_id','=',$icid)->firstorfail();
        if($product == null){
            return response()->json(["amt"=>-1]);
        }
        if($product->percentage_commission == 0)
        {
            return response()->json(["amt"=>number_format($product->amount,2),"percentage"=>$product->percentage_commission]);
        }
        $amts = ($product->percentage_commission * $amt)/100;
        return response()->json(["amt"=> number_format($amts,2),"percentage"=>$product->percentage_commission]);
    }



    private function getEdmundCarData()
    {
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->get('https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key='.config('insurance.edmund_key'))->getBody();

        $encode = json_decode($result->getContents());
        $make = $encode->makes;

        foreach($make as $m)
        {
            $mm = new CarMake();
            $mm->edmund_id = $m->id;
            $mm->name = $m->name;
            $mm->nicename = $m->niceName;
            if($mm->save())
            {
                //lets save the model
                foreach($m->models as $mo)
                {
                    $mmm = new CarModel();
                    $mmm->edmund_make_id = $mm->edmund_id;
                    $mmm->name = $mo->name;
                    $mmm->nicename = $mo->niceName;
                    $mmm->make_id= $mm->id;
                    $mmm->edmund_model_id = $mm->id;
                    if($mmm->save())
                    {
                        foreach($mo->years as $y)
                        {
                            $mmmm = new CarYear();
                            $mmmm->model_id = $mmm->model_id;
                            $mmmm->year = $y->year;
                            $mmmm->year_id = $y->id;
                            $mmmm->edmund_model_id = $mmm->edmund_model_id;
                            $mmmm->save();

                        }
                    }
                }
            }
           // dd(json_encode($mm));
        }

    }
}
