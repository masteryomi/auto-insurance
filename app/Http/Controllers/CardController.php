<?php

namespace App\Http\Controllers;

use App\Card;
use App\Purchase;
use App\User;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function validateCard ($c,$pid)
    {
        $user = \Auth::user();
        if($user == null)
        {
            return response()->json(["code"=>1,"msg"=>"user not logged in"]);
        }
        //check the card
        $purchase = Purchase::where('purchase_id',$pid)->where('user_id',$user->user_id)->first();
        if($purchase == null)
        {
            return response()->json(["code"=>1,"msg"=>"item cannot be found or does not exist"]);
        }
        if($purchase->product_id != config('insurance.thirdparty'))
        {
            return response()->json(["code"=>1,"msg"=>"Only third party product can be paid for using the scratch card"]);
        }
        $card = Card::where('card_number','=',$c)->first();
        if($card == null)
        {
            //increment the count for the this user
            if(!session()->has('card_try'))
            {
                session()->put("card_try",1);
            }else{
                $trys = session('card_try') + 1;
                session()->put('card_try',$trys);
            }
            $value = session()->get('card_try');
            if($value >= 3)
            {
                $usertoDisable = User::findorfail($user->user_id);
                $usertoDisable->status = 3;
                $usertoDisable->block_date=date('Y-m-d H:i:s', time());
                $usertoDisable->block_reason ='exceeded try times for card input';
                $usertoDisable->save();
                session()->forget('card_try');
                \Auth::logout();
                return response()->json(["code"=>2,"msg"=>"user locked out"]);
            }
            return response()->json(["code"=>1,"msg"=>"invalid card number entered"]);
        }
        if($card->status_id == 1)
        {
            return response()->json(["code"=>1,"msg"=>"card has not been enabled for use. Please contact administrator"]);
        }
        if($card->status_id == 3)
        {
            if($card->used_by == $user->user_id)
            {
                return response()->json(["code"=>1,"msg"=>"card was previously used by you"]);
            }
            return response()->json(["code"=>1,"msg"=>"card was previously used by another user. Please contact administrator"]);
        }

        $card->used_for = $pid;
        $card->used_by = $user->user_id;
        $card->used_at = date('Y-m-d H:i:s', time());
        $card->status_id = 3;

        if($card->save())
        {
            $purchase->approved_amount = $card->denomination;
            $purchase->switch_status = '00';
            $purchase->switch_reference = "SCP|".str_random(11)."|".time();
            $purchase->switch_description='Approved or completed successfully';
            $purchase->switch_response_time =date('Y-m-d H:i:s', time());
            if($purchase->save())
            {
                session()->forget('card_try');
                //send a mail of the payment to the customer
                return response()->json(["code"=>0,"msg"=>"Payment was successful"]);
            }
        }


    }
    private function generateCard()
    {
        $batchnumber = mt_rand(111111111,999999999);
        $count = 0;
        for($i = 1;$i<=1000;$i++)
        {
            $rand1 = mt_rand(11111,99999);
            $rand2 = mt_rand(1111,9999);
            $rand3 = mt_rand(111,999);
            $card = $rand1.$rand2.$rand3;

            $serial = mt_rand(11111111,99999999);

            $c = new Card();;
            $c->card_number = $card;
            $c->serial_number = $serial;
            $c->status_id = 1;
            $c->batchnumber = $batchnumber;
            $c->denomination = 5000;

            if($c->save())
            {
                $count++;
            }


        }

        dd($count);;



    }
}
