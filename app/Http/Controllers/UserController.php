<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function dashboard(){

        return view('user.dashboard')->with('active','udash');

    }

    public function logout()
    {
        \Auth::logout();
        return redirect('/login')->with('success','you have successfully logged out of your account');
    }

    public function profile()
    {
        return view('user.profile')->with('active','uprofile');
    }
}
