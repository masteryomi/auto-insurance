<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Role
{
    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            //the user is authenticated for sure
            $user =$this->auth->user();
            $role = $user->role()->first()->role_name;
            $url = explode("/",$request->path());
           // if($url[0] !="guest")
           // {
                if ($url[0]!= $role) {

                    return redirect($role.'/dashboard')->with('error', 'You are not authorised to access this function');
                }
           // }

        }catch(\Exception $e)
        {
            $this->auth->logout();
            return redirect()->guest('/')->with('error', 'You are not authorised to access this function '.$e->getMessage() );
        }
        return $next($request);
    }
}
