<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    protected $primaryKey='user_id';

    public function role()
    {
        return $this->belongsTo('App\Role','role_id','role_id');
    }

    public function purchases()
    {
        return $this->hasMany('App\Purchase','user_id');
    }
    public function fullname()
    {
        return $this->first_name ." ".$this->last_name;
    }

    public function cards()
    {
        return $this->hasMany('App\Card','used_by','user_id');
    }

    public function clients()
    {
        return $this->hasMany('App\User','registered_by','user_id');
    }
}
