<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalGovernment extends Model
{
    protected $table = 'local_govt';
    protected $primaryKey='id_no';
}
