<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceCompanyProduct extends Model
{
    protected $table = 'insurance_company_products';
    protected $primaryKey='icp_id';
}
