<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
class Purchase extends Model
{
    protected $table = 'purchases';
    protected $primaryKey='purchase_id';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User','user_id','user_id');
    }

    public function formatDate($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-M-Y h:i:s A');
    }

    public function carMake()
    {
        return $this->belongsTo('App\CarMake','make_id','id');
    }
    public function carModel()
    {
        return $this->belongsTo('App\CarModel','model_id','model_id');
    }
    public function idtype()
    {
        return $this->belongsTo('App\IdentificationType','idtype_id','idcardtype_id');
    }
    public function carYear()
    {
        return $this->belongsTo('App\CarYear','year_id','year_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Product','product_id','product_id');
    }
    public function insuranceCompany()
    {
        return $this->belongsTo('App\InsuranceCompany','insurance_company_id','insurance_company_id');
    }
}
