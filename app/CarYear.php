<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarYear extends Model
{
    protected $table = 'car_years';
    protected $primaryKey='car_year_id';
    public $timestamps = false;
}
