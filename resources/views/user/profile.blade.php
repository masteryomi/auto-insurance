@extends('layouts.user')
@section('page_title','Review Purchase')
@section('content')
    <section>

        <h3 class="heading">MY Profile</h3>
        <div class="row">

            <div class="col-md-12">
                <div class="table-responsive">
                    @include('partials._message')
                   <table class="table table-striped">
                       <tr><td>Full Name</td><td>{{\Auth::user()->fullname()}}</td></tr>
                       <tr><td>Address</td><td>{{\Auth::user()->address}}</td></tr>
                       <tr><td>State / Local Government</td><td>{{\Auth::user()->fullname()}}</td></tr>
                   </table>
                </div>
            </div>

        </div>

    </section>


@stop