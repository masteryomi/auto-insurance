@extends('layouts.user')
@section('page_title','Review Purchase')
@section('content')
    <section>
            <h3 class="heading">Welcome {{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</h3>
            <div class="row">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">PURCHASES</span><br>
                                    <h2>{{\Auth::user()->purchases()->count()}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">COMPLETED PURCHASES</span><br>
                                    <h2>{{\Auth::user()->purchases()->where('switch_status','00')->count()}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">FAILED PURCHASES</span><br>
                                    <h2>{{\Auth::user()->purchases()->whereNotNull('switch_status')->orWhere('switch_status','<>','00')->count()}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">PENDING PURCHASES</span><br>
                                    <h2>{{\Auth::user()->purchases()->whereNull('switch_status')->count()}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">CARDS USED</span><br>
                                    <h2>{{\Auth::user()->cards()->count()}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">ROLE</span><br>
                                    <h2>{{\Auth::user()->role()->first()->role_name}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">CLIENTS</span><br>
                                    <h2>{{\Auth::user()->clients()->count()}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default panel-auto">
                                <div class="panel-body" style="text-align: center !important;">
                                    <span class="text-center text-capitalize text-danger" style="font-size: 80%">COMMISSION</span><br>
                                    <h2>@if(\Auth::user()->commission_rate == null) None @else
                                            {{\Auth::user()->commission_rate}} %</h2>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

    </section>
@stop