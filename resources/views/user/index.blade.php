@extends('layouts.user')
@section('page_title','Review Purchase')
@section('content')
    <section>

            <h3 class="heading">MY Purchases</h3>
            <div class="row">

                <div class="col-md-12">
                    <div class="table-responsive">
                        @include('partials._message')
                        @foreach($purchases as $purchase)
                        <div class="job-listing">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="row">
                                        <div class="col-xs-2"><img src="{{asset('img/car.jpg')}}" alt=" " class="img-responsive"></div>
                                        <div class="col-xs-10">
                                            <h4 class="job__title"><a href="{{url('user/purchases/'.$purchase->purchase_id)}}">
                                                    {{$purchase->product()->first()->product_name}} for {{$purchase->carMake()->first()->name}}/
                                                    {{$purchase->carModel()->first()->name}}/{{$purchase->carYear()->first()->year}}</a></h4>
                                            <p class="job__company">
                                                @if($purchase->switch_status != null)
                                                <b>{{$purchase->switch_status}} - {{$purchase->switch_description}}</b>
                                                    @else
                                                <b>Pending Payment</b>
                                                    @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-10 col-xs-offset-2 col-sm-4 col-sm-offset-2 col-md-2 col-md-offset-0"><i class="fa fa-user job__location"></i>{{$purchase->user()->first()->fullname()}}</div>
                                <div class="col-xs-10 col-xs-offset-2 col-sm-4 col-sm-offset-0 col-md-3">
                                    <p>{{$purchase->formatDate($purchase->date_purchased)}}</p>
                                </div>
                                <div class="col-xs-10 col-xs-offset-2 col-sm-2 col-sm-offset-0 col-md-1">
                                    <div class="job__star"><a href="#" data-toggle="tooltip" data-placement="top" title="Save to favourites" class="job__star__link"><i class="fa fa-star"></i></a></div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>

    </section>


@stop