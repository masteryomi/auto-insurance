@extends('layouts.user')
@section('page_title','Review Purchase')
@section('content')
    <section>

            <h3 class="heading">Review Purchase</h3>
            <div class="row">

                <div class="col-md-12">
<div class="table-responsive">
    @include('partials._message')
    <table class="table table-bordered table-responsive">
        <tr><td>Transaction Reference</td><td>{{$purchase->payment_reference}}</td></tr>
        <tr><td>Date Generated</td><td>{{$purchase->formatDate($purchase->date_purchased)}}</td></tr>
        @if($purchase->percentage > 0)
            <tr><td>Percentage</td><td>{{$purchase->percentage}} %</td></tr>
        @endif
        <tr><td>Transaction Amount</td><td>&#8358; {{number_format($purchase->premium,2)}}</td></tr>
        <tr><td>Product Selected</td><td>{{$purchase->product()->first()->product_name}}</td></tr>
        <tr><td>Insurance Company Selected</td><td>{{$purchase->insuranceCompany()->first()->company_name}}</td></tr>
        <tr><td>Car Make/Model/Year/Color/Registration Number</td><td>{{$purchase->carMake()->first()->name}} /
                {{$purchase->carModel()->firstorfail()->name}} / {{$purchase->carYear()->firstorfail()->year}}
                /{{$purchase->car_color}}/ {{$purchase->regno}}</td></tr>
        <tr><td>Chassis Number / Engine Number</td><td>{{$purchase->chassis}} / {{$purchase->engine_no}}</td></tr>
        <tr><td>Identification Type/Identification Number</td><td>{{$purchase->idtype()->firstorfail()->cardtype}}/
                {{$purchase->idno}}</td></tr>

        @if($purchase->switch_status != null)
            <tr><td>Transaction Reference</td><td>
                        {{$purchase->switch_reference}}

                </td>
            </tr>
            <tr><td>Transaction Status</td><td>
                    <div class="alert @if($purchase->switch_status =='00')alert-success @else alert-danger @endif ">
                    {{$purchase->switch_status}} - {{$purchase->switch_description}}
                    </div>
                </td>
            </tr>
            @endif
        @if($purchase->switch_status == null or $purchase->switch_status !='00')
        <tr>
            <td>Choose Mode of Payment</td>
            <td>

                @if($purchase->product_id == config('insurance.thirdparty'))

                    <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-group btn-default">Use Scratch Card</a>
                @endif
                @if($purchase->switch_status == null)
                    <a href="{{url('user/purchases/debit-card/'.$purchase->purchase_id)}}" class="btn btn-group btn-default">Use Debit Card</a>
                @endif

        @if($purchase->switch_status != null and $purchase->switch_status !='00')
            <a href="{{url('user/purchases/retry-transaction/'.$purchase->purchase_id)}}" class="btn btn-group btn-danger">Retry Transaction</a>
            @endif
</td>
</tr>
@endif


</table>

</div>
    </div>


</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
<div class="modal-dialog" role="document">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Enter your Card Details</h4>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <input type="hidden" value="{{$purchase->purchase_id}}" id="pid"/>
            <input id="amount" type="text" placeholder="Card Number" class="form-control" name="cardNumber">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="pushcard">Pay</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop