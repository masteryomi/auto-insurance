@extends('layouts.user')
@section('page_title','Review Purchase')
@section('content')
    <section>
        <div class="container">
            <h3 class="heading">Pay with Scratch Card</h3>
            <div class="row">
                <div class="col-md-3">&nbsp;</div>
                <div class="col-md-9">
                    <table class="table table-bordered table-responsive">
                        <tr><td>Transaction Reference</td><td>{{$purchase->payment_reference}}</td></tr>
                        <tr><td>Date Generated</td><td>{{$purchase->date_purchased}}</td></tr>
                        @if($purchase->percentage > 0)
                            <tr><td>Percentage</td><td>{{$purchase->percentage}} %</td></tr>
                        @endif
                        <tr><td>Transaction Amount</td><td>&#8358; {{number_format($purchase->premium,2)}}</td></tr>
                        <tr><td>Product Selected</td><td>{{$purchase->product()->firstorfail()->product_name}}</td></tr>
                        <tr><td>Insurance Company Selected</td><td>{{$purchase->insuranceCompany()->firstorfail()->company_name}}</td></tr>
                        <tr><td>Car Make/Model/Year/Color/Registration Number</td><td>{{$purchase->carMake()->firstorfail()->name}} /
                                {{$purchase->carModel()->firstorfail()->name}} / {{$purchase->carYear()->firstorfail()->year}}
                                /{{$purchase->car_color}}/ {{$purchase->regno}}</td></tr>
                        <tr><td>Chassis Number / Engine Number</td><td>{{$purchase->chassis}} / {{$purchase->engine_no}}</td></tr>
                        <tr><td>Identification Type/Identification Number</td><td>{{$purchase->idtype()->firstorfail()->cardtype}}/
                                {{$purchase->idno}}</td></tr>

                        <tr>
                            <td>Mode of Payment</td>
                            <td>
                            Scratch Card
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <form action="post" class="" method="post">
                                    <input class="form-control" name="card_number" id="amount" placeholder="Enter the 12 digit number">
                                </form>
                            </td>
                        </tr>

                    </table>
                </div>

            </div>
        </div>
    </section>
@stop