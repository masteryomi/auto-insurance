@extends('layouts.user')
@section('page_title','Pay with Debit Card')
@section('content')
    <section>
      
            <h3 class="heading">Purchase With card</h3>
            <div class="row">

                <div class="col-md-12">
                    <div class="table-responsive">
                        <form class="form-horizontal" role="form" method="post" action="{{config("insurance.url")}}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table class="table table-bordered table-responsive">
                            <tr><td>Transaction Reference</td><td>{{$purchase->payment_reference}}</td></tr>
                            <tr><td>Date Generated</td><td>{{$purchase->date_purchased}}</td></tr>
                            @if($purchase->percentage > 0)
                                <tr><td>Percentage</td><td>{{$purchase->percentage}} %</td></tr>
                            @endif
                            <tr><td>Transaction Amount</td><td>&#8358; {{number_format($purchase->premium,2)}}</td></tr>
                            <tr><td>Product Selected</td><td>{{$purchase->product()->first()->product_name}}</td></tr>
                            <tr><td>Insurance Company Selected</td><td>{{$purchase->insuranceCompany()->first()->company_name}}</td></tr>
                            <tr><td>Car Make/Model/Year/Color/Registration Number</td><td>{{$purchase->carMake()->firstorfail()->name}} /
                                    {{$purchase->carModel()->firstorfail()->name}} / {{$purchase->carYear()->firstorfail()->year}}
                                    /{{$purchase->car_color}}/ {{$purchase->regno}}</td></tr>
                            <tr><td>Chassis Number / Engine Number</td><td>{{$purchase->chassis}} / {{$purchase->engine_no}}</td></tr>
                            <tr><td>Identification Type/Identification Number</td><td>{{$purchase->idtype()->first()->cardtype}}/
                                    {{$purchase->idno}}</td></tr>

                            <tr>
                                <td><img src="{{asset('img/vervemaster.png')}}" class="img-responsive"/> </td>

                                <td>
                                    <input type="hidden" name="gtpay_mert_id" value="{{config('insurance.merchant_id')}}" />
                                    <input type="hidden" name="gtpay_tranx_id" value="{{session('reference_no')}}" />
                                    <input type="hidden" name="gtpay_tranx_amt" value="{{session('totalamount') * 100}}" />
                                    <input type="hidden" name="gtpay_tranx_curr" value="{{config('insurance.currency')}}"/>
                                    <input type="hidden" name="gtpay_cust_id" value="{{Auth::user()->user_id}}" />
                                    <input type="hidden" name="gtpay_cust_name" value="{{Auth::user()->first_name}} {{Auth::user()->last_name}}" />
                                    <input type="hidden" name="gtpay_tranx_memo"  value="Payment for {{$purchase->product()->first()->product_name}} by {{Auth::user()->email}}" />
                                    <input type="hidden" name="gtpay_echo_data"   value="{{Auth::user()->user_id}}:{{session('totalamount')}}" />
                                    <input type="hidden" name="gtpay_gway_name" value="" />
                                    <input type="hidden" name="gtpay_hash" value="{{session('hash')}}" />
                                    <input type="hidden" name="gtpay_tranx_noti_url" value="{{config('insurance.return_url')}}" />
                                    {{--<input type="hidden" name="gtpay_hash" value="{{session('hash')}}" />--}}
                                    <button class="btn btn-success btn-lg pull-left" name="submit" value="pay"><i class="fa fa-shopping-cart"></i> Process Payment</button>
                                </td>
                            </tr>



                        </table>
                        </form>
                    </div>
                </div>

            </div>

    </section>


@stop