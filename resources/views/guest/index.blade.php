@extends('layouts.guest')
@section('page_title','Welcome to Online Auto Insurance')
@section('content')
    <section class="job-form-section job-form-section--image">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="job-form-box">
                        <h2 class="heading">It is  <span class="accent">easy</span> to insure your <span class="accent">car</span>.</h2>
                        <form id="job-main-form" method="get" action="{{url('/register')}}" class="job-main-form">
                            <div class="controls">
                                <div class="row">

                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label for="location">Cover</label>
                                            <select name="cover" data-required="true" id="cover" class="form-control">
                                                <option value="" selected="selected">Select your cover</option>
                                                @foreach($products as $i)
                                                    <option value="{{$i->product_id}}">{{$i->product_name}}</option>
                                                @endforeach
                                            </select> </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label for="profession">estimated car value</label>
                                            <input type="text" id="amount" name="amount" placeholder="e.g 300000" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="submit" class="btn btn-primary job-main-form__button">START</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <h3 class="heading">Our Insurance Premium Covers</h3>
            <p class="lead text-center margin-bottom--big">
                You can choose from any of the insurance covers to get started. </p>
            <div class="row packages">
                <div class="col-md-3">
                    <div class="package ">
                        <div class="package-header">
                            <h5>Third Party</h5>
                        </div>
                        <div class="price">
                            <div class="price-container">
                                <h4><span class="currency">&#8358;</span>5,000</h4><span class="period">/ year</span>
                            </div>
                        </div>
                        <ul>
                            <li><i class="fa fa-check"></i>Bodily harm to third party</li>
                            <li><i class="fa fa-check"></i>medical bills for third party</li>
                            <li><i class="fa fa-check"></i>legal fees</li>
                            <li><i class="fa fa-times"></i>repairs your car</li>
                            <li><i class="fa fa-times"></i>pays your medical bills</li>
                        </ul><a href="#" class="btn btn-primary">Sign Up</a>
                    </div>
                </div>
                <!-- end col-->
                <div class="col-md-3">
                    <div class="package ">
                        <div class="package-header">
                            <h5>Comprehensive (basic)</h5>
                        </div>
                        <div class="price">
                            <div class="price-container">
                                <h4><span class="currency"></span>3.5%</h4><span class="period">/ year</span>
                            </div>
                        </div>
                        <ul>
                            <li><i class="fa fa-check"></i>third party features</li>
                            <li><i class="fa fa-check"></i>repairs your car</li>
                            <li><i class="fa fa-check"></i>replaces your car</li>
                            <li><i class="fa fa-times"></i>car tracker</li>
                            <li><i class="fa fa-times"></i>auto buy back</li>
                        </ul><a href="#" class="btn btn-primary">Sign Up</a>
                    </div>
                </div>
                <!-- end col-->
                <div class="col-md-3">
                    <div class="package best-value">
                        <div class="package-header">
                            <h5>Comprehensive (BEST)</h5>
                            <div class="meta-text">Best Value</div>
                        </div>
                        <div class="price">
                            <div class="price-container">
                                <h4><span class="currency"></span>4.0%</h4><span class="period">/ year</span>
                            </div>
                        </div>
                        <ul>
                            <li><i class="fa fa-check"></i>third party features</li>
                            <li><i class="fa fa-check"></i>repairs your car</li>
                            <li><i class="fa fa-check"></i>replaces your car</li>
                            <li><i class="fa fa-times"></i>car tracker</li>
                            <li><i class="fa fa-check"></i>auto buy back</li>
                        </ul><a href="#" class="btn btn-primary">Sign Up</a>
                    </div>
                </div>
                <!-- end col-->
                <div class="col-md-3">
                    <div class="package ">
                        <div class="package-header">
                            <h5>Comprehensive</h5>
                        </div>
                        <div class="price">
                            <div class="price-container">
                                <h4><span class="currency"></span>5%</h4><span class="period">/ year</span>
                            </div>
                        </div>
                        <ul>
                            <li><i class="fa fa-check"></i>third party features</li>
                            <li><i class="fa fa-check"></i>repairs your car</li>
                            <li><i class="fa fa-check"></i>replaces your car</li>
                            <li><i class="fa fa-check"></i>car tracker</li>
                            <li><i class="fa fa-check"></i>auto buy back</li>
                        </ul><a href="#" class="btn btn-primary">Sign Up</a>
                    </div>
                </div>
                <!-- end col-->
            </div>
        </div>
    </section>
@stop