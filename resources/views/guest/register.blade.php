@extends('layouts.guest')
@section('page_title','Create An Account')
@section('content')
    <section>
        <div class="container">
            <h3 class="heading">Register in few steps to get started</h3>
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-8">
                    <div class="stepsForm">
                        <form method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="sf-steps">
                                <div class="sf-steps-content">
                                    <div>
                                        <span>1</span> &nbsp;Choose
                                    </div>
                                    <div>
                                        <span>2</span> &nbsp; Customize
                                    </div>
                                    <div>
                                        <span>3</span>&nbsp; Details
                                    </div>
                                    <div>
                                        <span>4</span>&nbsp; Review
                                    </div>

                                </div>
                            </div>
                            <div class="sf-steps-form sf-radius">

                                <ul class="sf-content">
                                    <!-- form step one -->
                                    <li>
                                        <div class="sf_columns column_6">
                                            <div class="sf-radio">
                                                Are you a new customer?
                                                <label><input type="radio" value="Y" name="newcustomer" data-required="true"><span></span> Yes</label>
                                                <label><input type="radio" value="N" name="newcustomer" data-required="true"><span></span> No</label>
                                            </div>
                                        </div>

                                    </li>
                                    <div class="hideothers">
                                        <li>
                                            <div class="sf_columns column_3">
                                                <label class="sf-select">
                                                    <select name="ic" data-required="true" id="ic">
                                                        <option value="" selected="selected">Select an Insurance Company</option>
                                                        @foreach($comp['ic'] as $i)
                                                            <option value="{{$i->insurance_company_id}}">{{$i->company_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="sf_columns column_3">
                                                <label class="sf-select">
                                                    <select name="cover" data-required="true" id="cover">
                                                        <option value="" selected="selected">Select your cover</option>
                                                        @foreach($comp['products'] as $i)
                                                            <option value="{{$i->product_id}}">{{$i->product_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span></span>
                                                </label>

                                            </div>
                                        </li>
                                        <li>
                                            <div class="sf_columns column_6">
                                                <input type="text" name="amount" id="amount" placeholder="What is your estimated car value?" data-required="true">
                                            </div>

                                        </li>
                                        <li>
                                            <div class="sf_columns column_3">
                                                <div class="sf-radio">
                                                    Is this car yours?
                                                    <label><input type="radio" value="Y" id='ownery' name="owner" data-required="true"><span></span> Yes</label>
                                                    <label><input type="radio" value="N" id='ownern' name="owner" data-required="true"><span></span> No</label>
                                                </div>
                                            </div>
                                            <div class="sf_columns column_3">
                                                <div class="sf-radio hideme">
                                                    Would you like to be our agent?
                                                    <label><input type="radio" value="Y" name="agent"><span></span> Yes</label>
                                                    <label><input type="radio" value="N" name="agent"><span></span> No</label>
                                                </div>
                                            </div>
                                        </li>
                                    </div>


                                </ul>

                                <ul class="sf-content">
                                    <!-- form step two -->
                                    <li>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="title" data-required="true" id="title">
                                                    <option value="" selected="selected">Select Title</option>
                                                    @foreach($comp['title'] as $i)
                                                        <option value="{{$i->title_id}}">{{$i->title_name}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="gender" data-required="true" id="gender">
                                                    <option value="" selected="selected">Select sex</option>
                                                    @foreach($comp['gender'] as $i)
                                                        <option value="{{$i->gender_id}}">{{$i->gender_name}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="First Name" data-required="true" id="fname" name="fname">
                                        </div>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="Last Name" data-required="true" id="lname" name="lname">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <input type="email" placeholder="Email Address" data-required="true" data-email="true" id="email" name="email">
                                        </div>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="Mobile Number" data-required="true" id="mnum" name="mnum">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <input type="password" placeholder="Enter a Password" data-required="true" data-confirm="true" id="p1" name="p1">
                                        </div>
                                        <div class="sf_columns column_3">
                                            <input type="password" placeholder="Confirm Password" data-required="true" data-confirm="true" id="p2" name="p2">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="states" data-required="true" id="states">
                                                    <option value="" selected="selected">Select your State</option>
                                                    @foreach($comp['state'] as $i)
                                                        <option value="{{$i->id_no}}">{{$i->state}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="lga" data-required="true" id="lga">
                                                    <option value="" selected="selected" id="showlga">Local Government</option>
                                                    {{--@foreach($comp['state'] as $i)--}}
                                                    {{--<option value="{{$i->id_no}}">{{$i->state}}</option>--}}
                                                    {{--@endforeach--}}
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_6">
                                            <textarea name="address" id="address" placeholder="Enter your home address"></textarea>
                                        </div>

                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="maritalstatus" data-required="true" id="maritalstatus">
                                                    <option value="" selected="selected">What is your marital status?</option>
                                                    @foreach($comp['ms'] as $i)
                                                        <option value="{{$i->marital_status_id}}">{{$i->marital_status}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="occupation" data-required="true" id="occupation">
                                                    <option value="" selected="selected">Select your occupation</option>
                                                    @foreach($comp['occupation'] as $i)
                                                        <option value="{{$i->occupation_id}}">{{$i->occupation_name}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="sf-content">
                                    <!-- form step tree -->
                                    <li>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="idtype" data-required="true" id="idtype">
                                                    <option value="" selected="selected">Select your identification mode</option>
                                                    @foreach($comp['idcard'] as $i)
                                                        <option value="{{$i->idcardtype_id}}">{{$i->cardtype}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="Identity card number" data-required="true" id="idno" name="idno" >

                                        </div>
                                    </li>


                                    <li>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="CHASSIS number"  data-required="true" id="cno" name="cno" >
                                        </div>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="Engine Number" name="eno" data-required="true" id="eno" >
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="Registration Number e.g KJA 506 DB"  data-required="true" id="regno" name="regno" >
                                        </div>
                                        <div class="sf_columns column_3">
                                            <input type="text" placeholder="Colour of Car e.g Red" name="carcolor" data-required="true" id="carcolor" >
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="cm" data-required="true" id="cm">
                                                    <option value="" selected="selected">Select make of Car</option>
                                                    @foreach($comp['cm'] as $i)
                                                        <option value="{{$i->id}}">{{$i->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="cmm" data-required="true" id="cmm">
                                                    <option value="" selected="selected" id="scmm">Select your Car Model</option>
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="cmy" data-required="true" id="cmy">
                                                    <option value="" selected="selected" id="scmy">Select your Car Year</option>
                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="sf_columns column_3">
                                            <label class="sf-select">
                                                <select name="yob" data-required="true" id="yob">
                                                    <option value="" selected="selected">Select your Year of Birth</option>
                                                    @for($i=1940;$i<=2000;$i++)
                                                        <option value="{{$i}}" >{{$i}}</option>
                                                    @endfor

                                                </select>
                                                <span></span>
                                            </label>
                                        </div>
                                    </li>

                                </ul>
                                <ul class="sf-content">
                                    <li>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Personal Information</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-responsive" style="background-color: #ffffff !important;font-size:90%;">
                                                    <tr>
                                                        <td class="font-label-size">Name</td>
                                                        <td><span id="ctitle"></span> <span id="cfname"></span> <span id="clname"></span></td>
                                                        <td class="font-label-size">Gender</td>
                                                        <td><span id="cgender"></span> </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size">Occupation</td>
                                                        <td><span id="coccupation"></span> </td>
                                                        <td class="font-label-size">Marital Status</td>
                                                        <td><span id="cmaritalstatus"></span> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size">Address</td>
                                                        <td colspan="3"><span id="caddress"></span></td>

                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size">State</td>
                                                        <td><span id="cstate"></span> </td>
                                                        <td class="font-label-size">Local Government</td>
                                                        <td><span id="clga"></span> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size">Email Address</td>
                                                        <td><span id="cemailaddress"></span></td>
                                                        <td class="font-label-size">Mobile Number</td>
                                                        <td><span id="cmnum"></span> </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size" >Year of Birth</td>
                                                        <td colspan="3"><span id="cyear"></span> </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Car Information</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-responsive" style="background-color: #ffffff !important;font-size:90%;">
                                                    <tr>
                                                        <td class="font-label-size">Car Make</td>
                                                        <td><span id="ccmake"></span></td>
                                                        <td class="font-label-size">Car Model</td>
                                                        <td><span id="ccmodel"></span> </td>


                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size" >Car Year</td>
                                                        <td><span id="ccyear"></span> </td>
                                                        <td class="font-label-size" >Car Color</td>
                                                        <td><span id="ccolor"></span> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size">Identification Type</td>
                                                        <td><span id="cidtype"></span></td>
                                                        <td class="font-label-size">Identification No</td>
                                                        <td><span id="cidno"></span> </td>


                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size">Chassis No</td>
                                                        <td ><span id="cchassis"></span></td>
                                                        <td class="font-label-size" >Engine No</td>
                                                        <td ><span id="cengine"></span> </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size" >Registration No</td>
                                                        <td colspan="3"><span id="cregno"></span> </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Your Insurance Cover Selection</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-responsive" style="background-color: #ffffff !important;font-size:90%;">
                                                    <tr>
                                                        <td class="font-label-size">Insurance Company</td>
                                                        <td><span id="cic"></span></td>
                                                        <td class="font-label-size">Cover</td>
                                                        <td colspan="3"><span id="ccover"></span> </td>

                                                    </tr>

                                                    <tr>
                                                        <td class="font-label-size">Percentage</td>
                                                        <td><span id="cpercent"></span></td>
                                                        <td class="font-label-size">Premium Amount</td>
                                                        <td><span id="cpremium"></span> </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="font-label-size" >Estimated Amount</td>
                                                        <td colspan="3"><span id="camount" ></span> </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                    </li>

                                    </ul>
                            </div>

                            <div class="sf-steps-navigation sf-align-right hideothers">
                                <span id="sf-msg" class="sf-msg-error"></span>
                                <button id="sf-prev" type="button" class="sf-button">Previous</button>
                                <button id="sf-next" type="button" class="sf-button">Next</button>
                            </div>
                        </form>
                    </div>
                    <!--STEPS FORM END -------------- -->
                </div>
                <div class="col-md-2">&nbsp;</div>
            </div>
        </div>
    </section>
@stop