@extends('layouts.guest')
@section('page_title','Create An Account')
@section('content')
    <section>
        <div class="container">
            <h3 class="heading">Login to your account</h3>
            <div class="row">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-4">
                    @include('partials._message')
                    <form action="{{url('/user-login')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input id="email_modal" type="email" placeholder="email address" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <input id="password_modal" type="password" placeholder="password" class="form-control" name="password">
                        </div>
                        <p class="text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                        </p>
                    </form>
                </div>
                <div class="col-md-4">&nbsp;</div>
            </div>
        </div>
    </section>
@stop