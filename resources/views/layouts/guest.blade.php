<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Auto Insurance - @yield('page_title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{{ asset('css/bootstrap.min.css')}}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{{ asset('css/font-awesome.min.css')}}}">
    <!-- Google fonts - Roboto for copy, Montserrat for headings-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <!-- owl carousel-->
    <link rel="stylesheet" href="{{{ asset('css/owl.carousel.css')}}}">
    <link rel="stylesheet" href="{{{ asset('css/owl.theme.css')}}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{{ asset('css/style.default.css')}}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{{ asset('css/custom.css')}}}">
    <link rel="stylesheet" href="{{{ asset('css/waitMe.css')}}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{{ asset('js/jquery-1.11.0.min.html')}}}"><\/script>')</script>

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<!-- navbar-->
<header class="header">
    <div role="navigation" class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header"><a href="index-2.html" class="navbar-brand"><img src="img/logo.png" alt="logo" class="hidden-xs hidden-sm"><img src="img/logo-small.png" alt="logo" class="visible-xs visible-sm"><span class="sr-only">Go to homepage</span></a>
                <div class="navbar-buttons">
                    <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
                </div>
            </div>
            <div id="navigation" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    @if ($active == 'index')

                        <li class="active"><a href="{{url('/')}}">Home</a></li>
                        @else
                        <li class=""><a href="{{url('/')}}">Home</a></li>

                    @endif
                        @if ($active == 'about')
                            <li class="active"><a href="{{url('/about-us')}}">About Us</a></li>
                            @else
                        <li class=""><a href="{{url('/about-us')}}">About Us</a></li>

                        @endif
                        @if ($active == 'hiw')
                            <li class="active"><a href="{{url('/how-it-works')}}">how it works</a></li>
                        @else
                            <li class=""><a href="{{url('/how-it-works')}}">how it works</a></li>

                        @endif
                        @if ($active == 'sp')
                            <li class="active"><a href="{{url('/sales-point')}}">Sales Point</a></li>
                        @else
                            <li class=""><a href="{{url('/sales-point')}}">Sales Point</a></li>

                        @endif
                        @if ($active == 'contact')
                            <li class="active"><a href="{{url('/contact')}}">Contact</a></li>
                        @else
                            <li class=""><a href="{{url('/contact')}}">Contact</a></li>

                        @endif

                </ul><a href="#" data-toggle="modal" data-target="#login-modal" class="btn navbar-btn btn-white"><i class="fa fa-sign-in"></i>Log in</a>
            </div>
        </div>
    </div>
</header>
<!-- *** LOGIN MODAL ***_________________________________________________________
-->
<div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">X</button>
                <h4 id="Login" class="modal-title">Customer login</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/user-login')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <input id="email_modal" type="email" placeholder="email address" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <input id="password_modal" type="password" placeholder="password" class="form-control" name="password">
                    </div>
                    <p class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                    </p>
                </form>
                <p class="text-center text-muted"><a href="{{url('register')}}">Create an account Here</a> </p>
                </div>
        </div>
    </div>
</div>
<!-- *** LOGIN MODAL END ***-->
@yield('content')


<footer class="footer">

    <div class="footer__copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>&copy;2016 Online Auto Insurance</p>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Javascript files-->

<script src="{{{ asset('js/bootstrap.min.js')}}}"></script>
<script src="{{{ asset('js/jquery.cookie.js')}}}"> </script>
<script src="{{{ asset('js/owl.carousel.min.js')}}}"></script>
<script src="{{{ asset('js/front.js')}}}"></script>
<script src="{{{ asset('js/waitMe.js')}}}"></script>
<script src="{{{ asset('js/autoinsurance.js')}}}"></script>
{{--<script>--}}
    {{--(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=--}}
            {{--function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;--}}
        {{--e=o.createElement(i);r=o.getElementsByTagName(i)[0];--}}
        {{--e.src='http://www.google-analytics.com/analytics.js';--}}
        {{--r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));--}}
    {{--ga('create','UA-XXXXX-X');ga('send','pageview');--}}
{{--</script>--}}
</body>
</html>