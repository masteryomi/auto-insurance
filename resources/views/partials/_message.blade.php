@if(count($errors))
    <div class="alert alert-danger">

        <div>
            <h4>The following errors occurred</h4>
            <ul>
                @foreach ($errors->all() as $error)

                    {{ $error }}<br>

                @endforeach
            </ul>
        </div>

        <span class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times-circle"></i></span>
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">{{session()->get('error')}}</div>
@endif
@if(session()->has('success'))
    <div class="alert alert-success">{{session()->get('success')}}</div>
@endif
@if(session()->has('status'))
    <div class="alert alert-success">{{session()->get('status')}}</div>
@endif
