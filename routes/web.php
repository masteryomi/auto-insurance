<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "GuestController@index");
Route::get('/register', "GuestController@register");
Route::post('/create-new-account', "GuestController@createAccount");
Route::get('/login', "GuestController@login");
Route::get('/lga/{id}', "GuestController@getLga")->where('id', '[0-9]+');
Route::get('/premium-price/{icid}/{type}/{amt}', "GuestController@getPrice")
    ->where('icid', '[0-9]+')
    ->where('type', '[0-9]+')
    ->where('amt', '[0-9.]+');
//Route::get('/makes', "GuestController@getEdmundCarData");
Route::get('/car-model/{id}', "GuestController@getModel")->where('id', '[0-9]+');
Route::get('/car-year/{id}', "GuestController@getYear")->where('id', '[0-9]+');
Route::post('/user-login', "GuestController@login_post");

Route::group(['middleware' => ['auth','role']], function () {
    Route::get('/user/purchases','PurchaseController@index');
    Route::get('/user/dashboard','UserController@dashboard');
    Route::get('/user/purchases/{id}', "PurchaseController@purchases")->where('id', '[0-9]+');
    Route::get('/user/purchases/debit-card/{id}','PurchaseController@payUsingCard')->where('id','[0-9]+');
    Route::get('/user/purchases/scratch-card/{c}/{p}','CardController@validateCard')->where('c','[0-9]+')->where('d','[0-9]+');
    Route::post('/user/purchases/complete','PurchaseController@complete');//
    Route::get('user/purchases/retry-transaction/{id}','PurchaseController@retryTransactioon')->where('id','[0-9]+');
    Route::get('user/logout','UserController@logout');
    Route::get('user/profile','UserController@profile');
});
