$(document).ready(function() {
    $("#amount").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $.fn.digits = function(){
        return this.each(function(){
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
        })
    }
   // $("#cardNumber").digits();
    $('#myModal').on('shown.bs.modal', function () {

        $('#amount').focus();
    });
    $('#pushcard').click(function(){
        var numb = $('#amount').val();
        if(numb == '')
        {
            alert('Please enter 12 digit card number');
            return;
        }
        $('#amount').val("Please wait...we are validating and applying your card");
        var piid = $('#pid').val();
        $.ajax({
            url: '/user/purchases/scratch-card/'+numb+'/'+piid,
            type: 'GET',
            //data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
console.log(data);
               // $.each(data,function(e,v){
                    var code = data.code;
                    var msg = data.msg;
                $('#amount').val('');
                    if(code == "0")
                    {
                        alert("Payment completed. Click ok and we will redirect you");
                        window.location='/user/purchases/'+piid;
                    }else if (code == "2")
                    {
                        window.location='/';
                    }else{
                        alert(msg);
                    }
                //})
            }
        });
    });
    $("#amount").digits();
    $('.hideme').hide();
    $('.hideothers').hide();
    $('input[type=radio][name=owner]').change(function() {
        if (this.value == 'N') {
            $('.hideme').show();
        }
        else if (this.value == 'Y') {
            $('.hideme').hide();
        }else{
            $('.hideme').hide();
        }
    });
    $('input[type=radio][name=newcustomer]').change(function() {
        if (this.value == 'N') {
            $('.hideothers').hide();
            $('#login-modal').modal({});
        }
        else if (this.value == 'Y') {
            $('.hideothers').show();
        }else{
            $('.hideothers').hide();
        }
    });


        $.fn.stepsForm = function (t) {
            var e = {
                    width: "100%",
                    active: 0,
                    errormsg: "Check faulty fields.",
                    sendbtntext: "Create Account",
                    posturl: "core/demo_steps_form.php",
                    theme: "default"
                },
                t = $.extend(e, t);
            return this.each(function () {
                function e(e) {
                    for (requredcontrol = !1, i = 0; i < e; i++) f.find(".sf-steps-form>ul").eq(i).find(":input").each(function () {
                        "true" == $(this).attr("data-required") && "" == $(this).val() ? ($(this).addClass("sf-error"), requredcontrol = !0) : "true" != $(this).attr("data-required") || "radio" != $(this).attr("type") && "checkbox" != $(this).attr("type") ? "" != $(this).val() && ("true" == $(this).attr("data-email") && 0 == r($(this).val()) && ($(this).addClass("sf-error"), requredcontrol = !0), "true" == $(this).attr("data-number") && isNaN($(this).val()) && ($(this).addClass("sf-error"), requredcontrol = !0), "true" == $(this).attr("data-confirm") && (h.push($(this).val()), u.push($(this)), a())) : "radio" == $(this).attr("type") ? $(this).parent().parent().find("input[type='radio']:checked").length < 1 && ($(this).addClass("sf-error"), requredcontrol = !0) : $(this).parent().parent().find("input[type='checkbox']:checked").length < 1 && ($(this).addClass("sf-error"), requredcontrol = !0)
                    });
                    d(), h.length = 0, requredcontrol ? f.find("#sf-msg").html(t.errormsg) : (o(), u.length = 0, t.active = e, t.active > c - 1 ? (t.active--, f.find("#sf-msg").text("")) : (n(), requredcontrol = !1, f.find("#sf-msg").text("")))
                }

                function s() {
                    f.width() > 500 ? (f.find(".column_1").css({
                        width: "16.666666667%",
                        "margin-bottom": "0px"
                    }), f.find(".column_2").css({
                        width: "33.333333334%",
                        "margin-bottom": "0px"
                    }), f.find(".column_3").css({
                        width: "50%",
                        "margin-bottom": "0px"
                    }), f.find(".column_4").css({
                        width: "66.666666667%",
                        "margin-bottom": "0px"
                    }), f.find(".column_5").css({
                        width: "83.333333334%",
                        "margin-bottom": "0px"
                    }), f.find(".column_6").css({
                        width: "100%",
                        "margin-bottom": "0px"
                    }), f.find(".sf-content>li").css({
                        "margin-bottom": "2rem"
                    }), f.find(".sf-steps-content").removeClass("sf-steps-center"), f.find(".sf-steps-navigation").removeClass("sf-align-center").addClass("sf-align-right")) : (f.find(".column_1").css({
                        width: "100%",
                        "margin-bottom": "2rem"
                    }), f.find(".column_2").css({
                        width: "100%",
                        "margin-bottom": "2rem"
                    }), f.find(".column_3").css({
                        width: "100%",
                        "margin-bottom": "2rem"
                    }), f.find(".column_4").css({
                        width: "100%",
                        "margin-bottom": "2rem"
                    }), f.find(".column_5").css({
                        width: "100%",
                        "margin-bottom": "2rem"
                    }), f.find(".column_6").css({
                        width: "100%",
                        "margin-bottom": "2rem"
                    }), f.find(".sf-content>li").css({
                        "margin-bottom": "0px"
                    }), f.find(".sf-steps-content").addClass("sf-steps-center"), f.find(".sf-steps-navigation").removeClass("sf-align-right").addClass("sf-align-center"))
                }

                function n() {
                    f.find(".sf-steps-content>div").removeClass("sf-active"), f.find(".sf-steps-form>.sf-content").css({
                        display: "none"
                    }), f.find(".sf-steps-form>ul").eq(t.active).fadeIn(), f.find(".sf-steps-content>div").eq(t.active).addClass("sf-active"), f.find("#sf-next").text(t.active == c - 1 ? t.sendbtntext : l), f.find("#sf-prev").css(0 == t.active ? {
                        display: "none"
                    } : {
                        display: "inline-block"
                    })
                }

                function r(t) {
                    var e = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return e.test(t)
                }

                function a() {
                    var t = h[0];
                    for (index = 0; index < h.length; index++) h[index] != t && (requredcontrol = !0), fisrctval = h[index]
                }

                function d() {
                    for (index = 0; index < u.length; index++) u[index].addClass("sf-error")
                }

                function o() {
                    for (index = 0; index < u.length; index++) u[index].removeClass("sf-error")
                }

                var f = $(this),
                    c = f.find(".sf-steps-content").find("div").length,
                    l = f.find("#sf-next").text(),
                    h = new Array,
                    u = new Array;
                f.css({
                    width: t.width
                }), f.addClass("sf-theme-" + t.theme), n(), f.find(":input").on("click", function () {
                    var t = $(this).parent().parent().find("input").attr("type");
                    "radio" == t || "checkbox" == t ? $(this).parent().parent().find("input").removeClass("sf-error") : $(this).removeClass("sf-error")
                }), f.find(".sf-steps-content>div").on("click", function () {
                    "sf-active" != $(this).attr("class") && (t.active > $(this).index() ? (t.active = $(this).index(), n()) : e($(this).index()))
                }), f.find("#sf-next").on("click", function () {
                    requredcontrol = !1, f.find(".sf-steps-form>ul").eq(t.active).find(":input").each(function () {
                        "true" == $(this).attr("data-required") && "" == $(this).val() ? ($(this).addClass("sf-error"), requredcontrol = !0) : "true" != $(this).attr("data-required") || "radio" != $(this).attr("type") && "checkbox" != $(this).attr("type") ? "" != $(this).val() && ("true" == $(this).attr("data-email") && 0 == r($(this).val()) && ($(this).addClass("sf-error"), requredcontrol = !0), "true" == $(this).attr("data-number") && isNaN($(this).val()) && ($(this).addClass("sf-error"), requredcontrol = !0), "true" == $(this).attr("data-confirm") && (h.push($(this).val()), u.push($(this)), a())) : "radio" == $(this).attr("type") ? $(this).parent().parent().find("input[type='radio']:checked").length < 1 && ($(this).addClass("sf-error"), requredcontrol = !0) : $(this).parent().parent().find("input[type='checkbox']:checked").length < 1 && ($(this).addClass("sf-error"), requredcontrol = !0)
                    }), d(), h.length = 0, requredcontrol ? f.find("#sf-msg").html(t.errormsg) : (o(), u.length = 0, f.find("#sf-next").text() == t.sendbtntext ?
                        (
                            f.find("#sf-msg").text(""),
                                run_waitMe($('.stepsForm > form'), 1, 'rotateplane'),
                        $.ajax({
                        type: "POST",
                        url: t.posturl,
                        data: f.find("form").serialize()
                    }).success(function (t) {
                            console.log(t);
                            $('.stepsForm > form').waitMe('hide');
                            try{
                                var tt = parseInt(t);
                                if(tt>0)
                                {
                                    window.location='/user/purchases/'+tt;
                                }else{
                                    window.location='/user/purchases';
                                }
                            }catch(e){
                                alert('Unable to create your account at this time. Please try again');
                            }

                    }).error(function(k){
                            console.log(k);
                            $('.stepsForm > form').waitMe('hide');
                            var respK = JSON.parse(k.responseText);
                            var errMsg = 'The following error occurred :\n';
                            $.each(respK,function(k,v){
                            errMsg += v[0]+"\n";
                            })
                            alert(errMsg);
                        })) : (t.active++, t.active > c - 1 ? (t.active--, f.find("#sf-msg").text("")) : (n(), requredcontrol = !1, f.find("#sf-msg").text(""))))
                }), f.find("#sf-prev").on("click", function () {
                    t.active--, t.active < 0 ? t.active++ : (n(), requredcontrol = !1)
                }), s(), $(window).resize(function () {
                    s()
                })
            })
        }

    $(".stepsForm").stepsForm({
        width			:'100%',
        active			:0,
        errormsg		:'Please fill in the required fields',
        sendbtntext		:'Create Account',
        posturl			:'/create-new-account',
        theme			:'red',
    });

    $('#states').change(function(){
        $('#showlga').html('Loading LGAs...');
        $.ajax({
            url: '/lga/'+$(this).val(),
            type: 'GET',
            //data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
                $('#lga').html('');
                $('#lga').append('<option id="showlga" selected="selected"></option>');
                $('#showlga').html('Please select your Local Government');
                $.each(data,function(e,v){
                    $('#lga').append("<option value='"+ v.id_no+"'>"+ v.local_govt+"</option>");
                })
            }
        });
    });
    $('#cm').change(function(){
        $('#scmm').html('Loading Car Model...');
        $.ajax({
            url: '/car-model/'+$(this).val(),
            type: 'GET',
            //data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
                $('#cmm').html('');
                $('#cmm').append('<option id="scmm" selected="selected"></option>');
                $('#scmm').html('Please select your Car Model');
                $.each(data,function(e,v){
                    $('#cmm').append("<option value='"+ v.model_id+"'>"+ v.name+"</option>");
                })
            }
        });
    });
    $('#cmm').change(function(){
        $('#scmy').html('Loading Car Year...');
        $.ajax({
            url: '/car-year/'+$(this).val(),
            type: 'GET',
            //data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
                $('#cmy').html('');
                $('#cmy').append('<option id="scmy" selected="selected"></option>');
                $('#scmy').html('Please select your Car Year');
                $.each(data,function(e,v){
                    $('#cmy').append("<option value='"+ v.car_year_id+"'>"+ v.year+"</option>");
                })
            }
        });
    });
    $('#sf-next').click(function(){
        if($(this).text() !='Create Account') {
            if ($('#fname').val() != "") {
                $('#cfname').html($('#fname').val());
            }
            if ($('#lname').val() != "") {
                $('#clname').html($('#lname').val());
            }
            if ($('#title option:selected').val() != "") {
                $('#ctitle').html($('#title option:selected').text());
            }
            if ($('#gender option:selected').val() != "") {
                $('#cgender').html($('#gender option:selected').text());
            }
            $('#ccover').html($('#cover option:selected').text());
            $('#cic').html($('#ic option:selected').text());
            $('#camount').html($('#amount').val());
            $('#cemailaddress').html($('#email').val());
            $('#caddress').html($('#address').val());
            $('#cmnum').html($('#mnum').val());
            $('#ccolor').html($('#carcolor').val());
            $('#cchassis').html($('#cno').val());
            $('#cengine').html($('#eno').val());
            $('#cyear').html($('#yob').val());
            $('#cregno').html($('#regno').val());
            $('#cstate').html($('#states option:selected').text());
            $('#cyob').html($('#yob option:selected').text());
            $('#clga').html($('#lga option:selected').text());
            $('#cmaritalstatus').html($('#maritalstatus option:selected').text());
            $('#coccupation').html($('#occupation option:selected').text());
            $('#ccmake').html($('#cm option:selected').text());
            $('#ccmodel').html($('#cmm option:selected').text());
            $('#ccyear').html($('#cmy option:selected').text());
            $('#cidtype').html($('#idtype option:selected').text());
            $('#cidno').html($('#idno').val());
            $('#cpremium').html('Loading price');
            $('#cpercent').html('Loading percentage');
            $.ajax({
                url: '/premium-price/' + $('#ic').val() + '/' + $('#cover').val() + '/' + $('#amount').val(),
                type: 'GET',
                //data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                success: function (data) {
                    $('#cpercent').html(data.percentage + '%');
                    $('#cpremium').html(data.amt);
                }
            });
        }
    });
    $('#waitMe_ex').click(function(){
        //run_waitMe($('.stepsForm > form'), 1, 'rotateplane');
    });
    function run_waitMe(el, num, effect){
        text = 'Please wait...';
        fontSize = '';
        switch (num) {
            case 1:
                maxSize = '';
                textPos = 'vertical';
                break;
            case 2:
                text = '';
                maxSize = 30;
                textPos = 'vertical';
                break;
            case 3:
                maxSize = 30;
                textPos = 'horizontal';
                fontSize = '18px';
                break;
        }
        console.log(effect)
        el.waitMe({
            effect: effect,
            text: text,
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            maxSize: maxSize,
            source: 'img.svg',
            textPos: textPos,
            fontSize: fontSize,
            onClose: function() {}
        });
    }


});