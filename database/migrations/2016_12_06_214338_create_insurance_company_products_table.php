<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceCompanyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_company_products', function (Blueprint $table) {
            $table->increments('icp_id');
            $table->integer('product_id');
            $table->integer('insurance_company_id');
            $table->timestamp('created_at');
            $table->decimal('percentage_commission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_company_products');
    }
}
