<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('card_id');
            $table->string('card_number');
            $table->string('serial_number');
            $table->timestamp('created_at');
            $table->integer('status_id');
            $table->timestamp('activated_at');
            $table->timestamp('used_at');
            $table->integer('used_by');
            $table->integer('created_by');
            $table->integer('activated_by');
            $table->integer('denomination');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
