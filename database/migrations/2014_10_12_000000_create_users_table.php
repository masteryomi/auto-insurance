<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->integer('role_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name');
            $table->string('mobile_number')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
            $table->integer('status');
            $table->string('confirmation_token');
            $table->decimal('commission_rate')->nullable();
            $table->integer('gender_id');
            $table->integer('registered_by')->nullable();
            $table->integer('state_id');
            $table->integer('lga_id');
            $table->integer('occupation_id');
            $table->integer('marital_status_id');
            $table->integer('makes_id');
            $table->integer('identification_type_id');
            $table->string('identification_number');
            $table->string('identification_path');
            $table->integer('title_id');
            $table->integer('require_password_change');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
